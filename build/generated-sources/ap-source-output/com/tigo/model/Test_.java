package com.tigo.model;

import java.math.BigDecimal;
import java.math.BigInteger;
import javax.annotation.Generated;
import javax.persistence.metamodel.SingularAttribute;
import javax.persistence.metamodel.StaticMetamodel;

@Generated(value="EclipseLink-2.5.2.v20140319-rNA", date="2019-01-07T14:09:56")
@StaticMetamodel(Test.class)
public class Test_ { 

    public static volatile SingularAttribute<Test, BigDecimal> id;
    public static volatile SingularAttribute<Test, String> telefono;
    public static volatile SingularAttribute<Test, BigInteger> dpi;
    public static volatile SingularAttribute<Test, String> nombre;

}