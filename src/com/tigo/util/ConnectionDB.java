/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tigo.util;

import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;

/**
 *
 * @author Tier3
 */
public class ConnectionDB {
    public final String persistenceTestName = "CrudJpaTestPU";
    
    public String getPersistenceTestName() {
        return persistenceTestName;
    }      
}
