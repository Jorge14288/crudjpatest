/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tigo.main;

import com.tigo.dao.TestDao;
import com.tigo.impl.ITest;
import com.tigo.model.Test;
import java.math.BigDecimal;
import java.math.BigInteger;
import java.util.List;
import javax.persistence.EntityManager;

/**
 *
 * @author Tier3
 */
public class CrudJpaTest {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
       
       // Create();
       // Update();
       // Delete();
        List<Test> lista = getAllTest();
    }
    private static EntityManager entityManager;

    public static List<Test> getAllTest() {
        ITest dao = new TestDao();
        return dao.getAll();
    }

    public static void Create() {
        Test test = new Test();
        test.setNombre("Test JPA2");
        test.setTelefono("78974564");
        test.setDpi(new BigInteger("7897"));
        test.setId(new BigDecimal("5"));
        ITest dao = new TestDao();
        dao.Create(test);
    }

    public static void Update() {
        Test test = new Test();
        ITest dao = new TestDao();
        test = dao.FindById(new BigDecimal("5"));
        test.setNombre("Test JPA2 modificado");
        dao = new TestDao();
        dao.Update(test);
    }
        public static void Delete() {
        Test test = new Test();
        ITest dao = new TestDao();
        test = dao.FindById(new BigDecimal("5"));
        test.setNombre("Test JPA2 modificado");
        dao = new TestDao();
        dao.Delete(test);
    }

}
