/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tigo.dao;

import com.tigo.impl.ITest;
import com.tigo.model.Test;
import java.util.List;
import com.tigo.util.ConnectionDB;
import java.math.BigDecimal;
import javax.persistence.EntityManager;
import javax.persistence.EntityManagerFactory;
import javax.persistence.Persistence;
import javax.persistence.TypedQuery;

/**
 *
 * @author Tier3
 */
public class TestDao implements ITest {

    private EntityManagerFactory emf = null;

    public EntityManager getEntityManager() {
        return emf.createEntityManager();
    }

    public TestDao() {
        ConnectionDB db = new ConnectionDB();
        emf = Persistence.createEntityManagerFactory(db.getPersistenceTestName());
    }

    @Override
    public List<Test> getAll() {
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            //List<Test> lista = em.createQuery("SELECT t FROM Test t").getResultList();
            TypedQuery<Test> query
                    = em.createNamedQuery("Test.findAll", Test.class);
            List<Test> lista = query.getResultList();
            em.getTransaction().commit();
            if (lista == null) {
                System.out.println("No Test found . ");
            } else {
                for (Test test : lista) {
                    System.out.print("Nombre= " + test.getNombre()
                            + ", Dpi" + test.getDpi() + ", Telefono="
                            + test.getTelefono());
                }
            }
            return lista;
        } finally {
            if (em != null) {
                em.close();
                emf.close();
            }
        }
    }

    @Override
    public boolean Create(Test test) {
        boolean respuesta = false;
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.persist(test);
            em.getTransaction().commit();
            return respuesta;
        } catch (Exception ex) {
            System.out.println("No se pudo crear el objeto " + ex.getMessage());
            ex.printStackTrace();
            return respuesta;
        } finally {
            if (em != null) {
                em.close();
            }
        }
    }

    @Override
    public boolean Update(Test test) {
        boolean respuesta = false;
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.merge(test);
            em.getTransaction().commit();
            return respuesta;
        } catch (Exception ex) {
            System.out.println("No se pudo crear el objeto " + ex.getMessage());
            ex.printStackTrace();
            return respuesta;
        } finally {
            if (em != null) {
                em.close();
                emf.close();
            }
        }
    }

    @Override
    public boolean Delete(Test test) {
        boolean respuesta = false;
        EntityManager em = null;
        try {
            em = getEntityManager();
            em.getTransaction().begin();
            em.remove(test);
            em.getTransaction().commit();
            return respuesta;
        } catch (Exception ex) {
            System.out.println("No se pudo crear el objeto " + ex.getMessage());
            ex.printStackTrace();
            return respuesta;
        } finally {
            if (em != null) {
                em.close();
                emf.close();
            }
        }
    }

    @Override
    public Test FindById(BigDecimal id) {
        EntityManager em = getEntityManager();
        try {
            return em.find(Test.class, id);
        } finally {
            em.close();
            emf.close();
        }
    }

}
