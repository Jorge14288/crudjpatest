/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.tigo.impl;
import com.tigo.model.Test;
import java.math.BigDecimal;
import java.util.List;

/**
 *
 * @author Tier3
 */
public interface ITest {
    
    public List<Test> getAll();
    public boolean Create(Test test);
    public boolean Update(Test test);
    public boolean Delete(Test test);
    public Test FindById(BigDecimal id);
    
}
